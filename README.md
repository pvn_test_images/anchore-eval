# anchore-eval



# Anchore Enterprise Evaluation

## Introduction

anchorectl is a command line interface tool for interacting with Anchore Enterprise.  This document covers the use of anchorectl specifically in automated pipelines.  For full documentation on this client, please refer to https://docs.anchore.com

## Quickstart Install

```curl -sSfL https://anchorectl-releases.anchore.io/anchorectl/install.sh | sh -s -- -b /usr/local/bin```

This will install the latest version into /usr/local/bin.  In pipeline usage, depending on tooling, you may need to install as a non-root user, in which case you can install in a location such as `${HOME}/.local/bin` or similar.  You may also need to augment your `${PATH}` depending on environment.
